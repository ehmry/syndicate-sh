#!/bin/bash

. $(dirname "$0")/../lib/syndicate.sh

service() {
    echo $ds_event
    case $(ds_project "$ds_event" ".^") in
        retract)
            echo 'Bye!'
            exit 0
            ;;
        *)
            ;;
    esac
}

ds_object service_entry service
echo 'Running' >&2
ds_connect '<stdio>'
